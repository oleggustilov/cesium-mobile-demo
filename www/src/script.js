function init() {

  var mapOL = new ol.Map({
        target: 'map',
        loadTilesWhileAnimating: true,
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([37.41, 8.82]),
            zoom: 4
        })
    });

    var ol3d = new olcs.OLCesium({
        map: mapOL
    });
    ol3d.setEnabled(true);

    var vectorSource = new ol.source.Vector({
        wrapX: false
    });

    var vectorLayer = new ol.layer.Vector({
        source: vectorSource,
        style: new ol.style.Style({
            image: new ol.style.Circle({
                radius: 5,
                stroke: new ol.style.Stroke({
                    color: 'RED',
                    width: 2
                })
            })
        })
    });

    mapOL.addLayer(vectorLayer);

    var drawInteraction = new ol.interaction.Draw({
        source: vectorSource,
        type: 'Point'
    });

    drawInteraction.on('drawstart', function(evt) {
        var coordinatesDrawing = ol.proj.transform(evt.feature.getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326');
        console.log("Coordinates where drawed (ol) : " + coordinatesDrawing);
    });

    mapOL.addInteraction(drawInteraction);

    mapOL.on('singleclick', function(evt) {
        var coordinatesClicked = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
        console.log("Coordinates where clicked (ol) : " + coordinatesClicked);
    });

    var scene = ol3d.getCesiumScene();
scene.mode = Cesium.SceneMode.COLUMBUS_VIEW;
    scene.canvas.addEventListener('click', function(e) {

        var mousePosition = new Cesium.Cartesian2(e.clientX, e.clientY);
        var ellipsoid = scene.globe.ellipsoid;
        var cartesian = scene.camera.pickEllipsoid(mousePosition, ellipsoid);

        if (cartesian) {
            var cartographic = ellipsoid.cartesianToCartographic(cartesian);
            var longitudeString = Cesium.Math.toDegrees(cartographic.longitude).toFixed(10);
            var latitudeString = Cesium.Math.toDegrees(cartographic.latitude).toFixed(10);

            console.log("Coordinates where clicked (cesium) : " + longitudeString + ", " + latitudeString);

        } else {
            console.log('Globe was not picked');
        }

        ol3d.getDataSourceDisplay().defaultDataSource.entities.add({
            position: cartesian,
            point : {
                pixelSize : 5,
                color : Cesium.Color.BLACK
            }
        });

     }, false);

}
